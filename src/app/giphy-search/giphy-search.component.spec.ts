import { GiphySearchComponent } from './giphy-search.component';
import { GiphyService } from '../shared/giphy/giphy.service';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { SharedModule } from '../shared/shared.module';
import { merge, of, Subject } from 'rxjs';
import { SearchResults } from '../shared/giphy/giphy.model';
import { delay, mergeMap } from 'rxjs/internal/operators';

describe('GiphySearchComponent', () => {
  let component: GiphySearchComponent;
  let fixture: ComponentFixture<GiphySearchComponent>;
  let giphyService: GiphyService;
  let mockGiphyService: Partial<GiphyService>;
  let searchSubject: Subject<string>;

  beforeEach(() => {
    searchSubject = new Subject<string>();

    mockGiphyService = {
      search$: searchSubject.asObservable(),
      getSearchResultsStream: (obs) => {
        return merge(...obs).pipe(
          mergeMap(() => of({} as SearchResults).pipe(delay(1000)))
        );
      },
      publishSearch: () => {
      }
    };

    TestBed.configureTestingModule({
      declarations: [
        GiphySearchComponent
      ],
      imports: [
        SharedModule
      ]
    });

    TestBed.overrideComponent(GiphySearchComponent, {
      set: {
        providers: [
          { provide: GiphyService, useValue: mockGiphyService }
        ]
      }
    });

    fixture = TestBed.createComponent(GiphySearchComponent);
    component = fixture.componentInstance;

    // TestBed.get does not work, because its not in the root
    giphyService = fixture.debugElement.injector.get(GiphyService);
  });

  describe('ngOnInit', () => {
    it('should set the search result stream', () => {
      component.ngOnInit();

      expect(component.searchResult$).toBeDefined();
    });
  });

  describe('search', () => {
    beforeEach(() => {
      spyOn(giphyService, 'publishSearch').and.callThrough();
    });

    it('should publish search text to service', () => {
      component.search('test');

      expect(giphyService.publishSearch).toHaveBeenCalledWith('test');
    });

    it('should not publish when search text is empty', () => {
      component.search('');

      expect(giphyService.publishSearch).not.toHaveBeenCalled();
    });
  });

  describe('loading', () => {
    it('should set loading true when search is published, and false when response received', fakeAsync(() => {
      component.ngOnInit();
      component.searchResult$.subscribe();
      expect(component.loading).toBeUndefined();

      searchSubject.next('test');

      tick(1000);
      fixture.detectChanges();
      expect(component.loading).toBe(true);

      tick(1000);
      fixture.detectChanges();
      expect(component.loading).toBe(false);
    }));


    it('should set loading true when load more is emitted, and false when response received', fakeAsync(() => {
      component.ngOnInit();
      component.searchResult$.subscribe();

      searchSubject.next('test');
      tick(2000);
      fixture.detectChanges();
      expect(component.loading).toBe(false);

      component.loadMore.emit(1);

      fixture.detectChanges();
      expect(component.loading).toBe(true);

      tick(1000);
      fixture.detectChanges();
      expect(component.loading).toBe(false);
    }));
  });
});
