import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, tap, withLatestFrom } from 'rxjs/internal/operators';
import { GiphyService } from '../shared/giphy/giphy.service';
import { SearchObject, SearchResults } from '../shared/giphy/giphy.model';

@Component({
  selector: 'giphy-search',
  templateUrl: './giphy-search.component.html',
  styleUrls: ['./giphy-search.component.scss'],
  providers: [GiphyService]
})
export class GiphySearchComponent implements OnInit {
  @Input() limit = 2;

  searchResult$: Observable<SearchResults>;
  // TODO Subject
  loadMore = new EventEmitter();
  loading: boolean;

  constructor(private giphyService: GiphyService) {
  }

  ngOnInit() {
    this.searchResult$ = this.giphyService.getSearchResultsStream([
        this.getSearchInputEventStream(),
        this.getLoadButtonEventStream()
      ],
      this.limit
    ).pipe(
      tap(() => this.loading = false)
    );
  }

  search(search: string): void {
    if (!search) {
      return;
    }

    this.giphyService.publishSearch(search);
  }

  private getSearchInputEventStream(): Observable<SearchObject> {
    return this.giphyService.search$.pipe(
      debounceTime(1000),
      distinctUntilChanged(),
      map(search => ({ search, offset: 0 })),
      tap(() => this.loading = true)
    );
  }

  private getLoadButtonEventStream(): Observable<SearchObject> {
    return this.loadMore.pipe(
      withLatestFrom(this.giphyService.search$),
      map(([offset, search]) => ({ search, offset })),
      tap(() => this.loading = true)
    );
  }
}
