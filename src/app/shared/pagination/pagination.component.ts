import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { SearchResults } from '../giphy/giphy.model';

@Component({
  selector: 'giphy-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaginationComponent {
  @Input() searchResults$: Observable<SearchResults>;
  @Input() loading: boolean;

  @Output() loadMore = new EventEmitter();
}
