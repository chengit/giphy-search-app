import { HttpClient } from '@angular/common/http';
import { of, throwError } from 'rxjs/index';
import { GiphySearchRequest, GiphySearchResponse } from './giphy.model';
import { GiphyResource } from './giphy.resource';


describe('GiphyResource', () => {
  let service: GiphyResource;
  let httpSpy: jasmine.SpyObj<HttpClient>;

  beforeEach(() => {
    const apiMock: GiphySearchResponse = {
      data: [],
      pagination: {
        count: 1,
        total_count: 2
      }
    } as GiphySearchResponse;

    httpSpy = jasmine.createSpyObj('HttpClient', ['get']);
    httpSpy.get.and.returnValue(of(apiMock));

    service = new GiphyResource(httpSpy as HttpClient);
  });

  describe('getGifs', () => {
    it('should call endpoint with given params', () => {
      const params: GiphySearchRequest = {
        search: 'test',
        limit: 2,
        offset: 1
      };

      service.getGifs(params);

      expect(httpSpy.get).toHaveBeenCalledWith(
        'http://api.giphy.com/v1/gifs/search?api_key=CdRKiCMbTnt9CkZTZ0lGukSczk6iT4Z6&limit=2&q=test&offset=1'
      );
    });

    it('should handle/catch error and return an usable response', () => {
      httpSpy.get.and.returnValue(throwError(of('test')));

      const params: GiphySearchRequest = {
        search: 'test',
        limit: 2,
        offset: 1
      };

      service.getGifs(params).subscribe((value) => {
        expect(value).toEqual({
          data: [],
          pagination: {},
          meta: {},
          error: 'Giphy API failed'
        } as GiphySearchResponse);
      });
    });

    it('should not call API when invalid request', () => {
      const params: any = {
        search: 1,
        limit: '10',
        offset: true
      };

      service.getGifs(params);

      expect(httpSpy.get).not.toHaveBeenCalled();
    });
  });
});
