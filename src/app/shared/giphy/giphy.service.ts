import { Injectable } from '@angular/core';
import { merge, Observable, Subject } from 'rxjs';
import { map, mergeMap, scan, shareReplay, withLatestFrom } from 'rxjs/internal/operators';
import { GiphySearchResponse, SearchObject, SearchResults } from './giphy.model';
import { GiphyResource } from './giphy.resource';

@Injectable()
export class GiphyService {
  private searchSubject = new Subject<string>();

  search$: Observable<string> = this.searchSubject.asObservable();

  constructor(private service: GiphyResource) {
  }

  publishSearch(search: string): void {
    this.searchSubject.next(search);
  }

  getSearchResultsStream(obs: Observable<SearchObject>[], limit: number): Observable<SearchResults> {
    return this.getServiceResponseStream(obs, limit).pipe(
      withLatestFrom(this.searchSubject),
      this.mapToSearchResults(),
      // TODO view specific logic
      this.accumulateSearchResults(),
      shareReplay()
    );
  }

  // this exposed method is not used in this application, but is also an usable feed for consumers wanting raw/unmodified data
  getServiceResponseStream(obs: Observable<SearchObject>[], limit: number): Observable<GiphySearchResponse> {
    return merge(
      ...obs
    ).pipe(
      mergeMap((params: SearchObject) => {
        return this.service.getGifs({ search: params.search, offset: params.offset, limit });
      }),
      shareReplay()
    );
  }

  // TODO Typing return
  private mapToSearchResults() {
    return map(([response, search]) => {
      return {
        // TODO validate response whether it matches our interface, maybe check voor http in string or so
        items: response.data.map(object => object.images.fixed_width.url),
        totalCount: response.pagination.total_count,
        count: response.pagination.count,
        searchText: search,
        error: response.error
      };
    });
  }

  // TODO Typing return
  private accumulateSearchResults() {
    return scan((accumulated: SearchResults, current: SearchResults) => {
      if (accumulated.searchText === current.searchText && !current.error) {
        accumulated.items = accumulated.items.concat(current.items);
        accumulated.count = accumulated.count + current.count;
        return accumulated;
      } else {
        return current;
      }
    });
  }
}

