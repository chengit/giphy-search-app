import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Gif, GiphySearchRequest, GiphySearchResponse, Meta, Pagination } from './giphy.model';
import { catchError, delay, mergeMap, retryWhen } from 'rxjs/internal/operators';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GiphyResource {
  private errorObject: GiphySearchResponse = {
    data: [] as Gif[],
    pagination: {} as Pagination,
    meta: {} as Meta,
    error: 'Giphy API failed'
  };

  constructor(private httpClient: HttpClient) {
  }

  getGifs(request: GiphySearchRequest): Observable<GiphySearchResponse> {
    // validate request for non Typescript consumers, defensive programming for not overloading service with unnecessary/invalid calls
    if (
      typeof request.search !== 'string' &&
      typeof request.offset !== 'number' &&
      typeof request.limit !== 'number'
    ) {
      return;
    }

    const params = `api_key=${environment.giphyApiKey}&limit=${request.limit}&q=${request.search}&offset=${request.offset}`;
    return this.httpClient
      .get<GiphySearchResponse>(`${environment.giphyApiUrl}?${params}`)
      .pipe(
        // TODO tap caching
        // TODO add retry with delay
        retryWhen(attempts => attempts.pipe(
          delay(1000),
          mergeMap((result, i) => {
            if (i < 3) {
              return of(result);
            }
            return throwError(result);
          }),
        )),
        catchError(() => of(this.errorObject)),
      );
  }
}

