import { of } from 'rxjs/index';
import { GiphySearchResponse, SearchObject, SearchResults } from './giphy.model';
import { GiphyResource } from './giphy.resource';
import { GiphyService } from './giphy.service';
import { map, skip } from 'rxjs/internal/operators';


describe('GiphyService', () => {
  let service: GiphyService;
  let giphyResourceSpy: jasmine.SpyObj<GiphyResource>;

  beforeEach(() => {
    giphyResourceSpy = jasmine.createSpyObj('GiphyResource', ['getGifs']);

    service = new GiphyService(giphyResourceSpy as GiphyResource);
  });

  describe('publishSearch', () => {
    it('should emit value to search observable', () => {
      const search = 'test';

      service.search$.subscribe((value) => {
        expect(value).toBe(search);
      });

      service.publishSearch(search);
    });
  });

  describe('getServiceResponseStream', () => {
    it('should return a stream with giphy response from api', () => {
      const searchStream = service.search$.pipe(map((value) => ({ search: value } as SearchObject)));
      const giphyResponse = {
        data: [
          {
            images: {
              fixed_height: {
                url: 'testurl'
              }
            }
          }
        ],
        pagination: {
          count: 1,
          total_count: 5
        },
        meta: {}
      } as GiphySearchResponse;

      giphyResourceSpy.getGifs.and.returnValue(of(giphyResponse));

      service.getServiceResponseStream([searchStream], 1).subscribe((value) => {
        expect(value).toBe(giphyResponse);
      });

      service.publishSearch('next');

      expect(giphyResourceSpy.getGifs).toHaveBeenCalledWith({ search: 'next', offset: undefined, limit: 1 });
    });
  });

  describe('getSearchResultsStream', () => {
    const giphyResponse = {
      data: [
        {
          images: {
            fixed_height: {
              url: 'testurl'
            }
          }
        }
      ],
      pagination: {
        count: 2,
        total_count: 5
      },
      meta: {}
    } as GiphySearchResponse;

    beforeEach(() => {
      giphyResourceSpy.getGifs.and.returnValue(of(giphyResponse));
    });

    it('should return search results from the service response stream', () => {
      const searchStream = service.search$.pipe(map(() => ({} as SearchObject)));
      const searchResults = {
        items: ['testurl'],
        count: 2,
        totalCount: 5,
        searchText: 'next',
        error: undefined
      } as SearchResults;

      service.getSearchResultsStream([searchStream], 1).subscribe((value) => {
        expect(value).toEqual(searchResults);
      });

      service.publishSearch('next');
    });

    it('should accumulate search results', () => {
      const searchStream = service.search$.pipe(map(() => ({} as SearchObject)));

      service.getSearchResultsStream([searchStream], 1).pipe(skip(1)).subscribe((value) => {
        expect(value.items.length).toBe(2);
        expect(value.count).toBe(4);
      });

      service.publishSearch('next');
      service.publishSearch('next');
    });

    it('should reset accumulation when search text is different from previous', () => {
      const searchStream = service.search$.pipe(map(() => ({} as SearchObject)));

      service.getSearchResultsStream([searchStream], 1).pipe(skip(2)).subscribe((value) => {
        expect(value.items.length).toBe(1);
        expect(value.count).toBe(2);
      });

      service.publishSearch('next');
      service.publishSearch('next');
      service.publishSearch('different');
    });
  });
});
