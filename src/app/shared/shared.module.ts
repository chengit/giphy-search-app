import { NgModule } from '@angular/core';
import { PaginationComponent } from './pagination/pagination.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  declarations: [
    PaginationComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  exports: [PaginationComponent]
})
export class SharedModule { }
